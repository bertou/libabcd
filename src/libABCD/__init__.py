__version__ = "2.2.0"

import configparser, json, logging, os, time
from .logutils import init_file_handler, set_formatter, set_handler_level, set_mqtt_log_topic
import paho.mqtt.client as mqtt


############################# global variables ################################
###############################################################################
### flags ###
initialized_flag = False
report_status_flag = False

### objects ###
mqttp = None
mqttl = None
logger = logging.getLogger('libABCD')
retained_messages = []
subscriptions = []
pid = str(os.getpid())

### configuration ###
mqtt_dict = {'host': 'localhost', 
             'port': 1883, 
             'username': None, 
             'password': None}
project_name = None
project_topic = ''
app_name = 'libABCD'
app_name_pid = app_name + pid

class ABCDError(Exception):
    pass


################################ private API ##################################
###############################################################################
def _format_msg(payload):
    try:
        return json.dumps({'from': app_name_pid, 'timestamp': time.time(), 
                           'payload': payload})
    except Exception:
        logger.exception(f'payload {payload} could not be formatted.')


def _mqtt_client(client_type):
    
    if client_type == 'listener':
        mqttc = mqttl
        
    elif client_type == 'publisher':
        mqttc = mqttp
        
    else:
        raise ValueError(f'client_type should be "listener" or "publisher", not {client_type}')

    return mqttc


########################### mqtt generic callbacks ############################
###############################################################################
def _on_connect(client: mqtt.Client, userdata, connect_flags, reason_code, properties):
    
    cid = client._client_id.decode()

    if reason_code == 0:

        if report_status_flag:
            # report on
            msg = _format_msg('on')
            client.publish(f'{project_topic}clients/{cid}', msg, retain=True,
                            qos=2)

    else:
        logger.error(f"mqtt client {cid} connection error, reason_code={reason_code}")

    # if the client is a listener, re-subscribe to all topics to catch changes
    # in retained messages
    if 'listener' in cid:
        for sub in subscriptions:
            client.subscribe(sub, qos=2)
    
    # if it is a publisher, re-send all retained messages
    else:
        resend_retained_messages()


def _on_disconnect(client, userdata, disconnect_flags, reason_code, properties):
    
    cid = client._client_id.decode()

    if reason_code != 0:
        logger.critical(f"mqtt client {cid} unexpected disconnection, reason_code={reason_code}")


def _pingpong_callback(msg, topic):
    publish('pongs')


##################### public API - new available functions ####################
###############################################################################
def setup_root_logger(console_level=None,
                      file_level=None,
                      file_dir=None,
                      mqtt_level=None,
                      mqtt_topic=None,
                      formatter=None):

    if console_level is not None: # create console handler
        set_handler_level('console', console_level)

    if file_level: # create file handler

        if file_dir is None:
            raise ABCDError('file_dir must be passed if file_level is not None')

        # file log directory
        os.makedirs(file_dir, exist_ok=True)

        # create file handler
        init_file_handler(os.path.join(file_dir, f'{app_name}.log'))
        set_handler_level('file', file_level)

    if mqtt_level is not None:  # modify ABCDHandler
        set_handler_level('mqtt', mqtt_level)

    if mqtt_topic is not None:  # modify MQTT log topic
        set_mqtt_log_topic(mqtt_topic)
    
    if formatter is not None:   # modify formatter for console and file
        set_formatter(formatter)


def init_mqtt_client(client_type: str, client_id=None):

    mqttc = _mqtt_client(client_type)

    if mqttc is not None:
        raise ABCDError(f'A MQTT {client_type} was already initialized. Run "disconnect_mqtt_client" first.')

    if client_type == 'listener':

        global mqttl

        if client_id is None:
            client_id = app_name_pid+'_listener'
        
        mqttl = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id=client_id)
        mqttc = mqttl

    elif client_type == 'publisher':

        global mqttp

        if client_id is None:
            client_id = app_name_pid
        
        mqttp = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id=client_id)
        mqttc = mqttp
    
    mqttc.on_connect = _on_connect
    mqttc.on_disconnect = _on_disconnect
    mqlogger = logging.getLogger(f'mqtt.{client_type}')
    mqttc.enable_logger(mqlogger)
    mqttc.username_pw_set(username=mqtt_dict['username'],
                          password=mqtt_dict['password'])
    if report_status_flag:
        mqttc.will_set(f'{project_topic}clients/{client_id}', None, retain=True)


def connect_mqtt_client(client_type):

    mqttc = _mqtt_client(client_type)

    if mqttc is None:
        raise ABCDError(f'MQTT {client_type} not initialized. Run "init_mqtt_client" first.')

    mqttc.connect(host=mqtt_dict['host'], port=mqtt_dict['port'])
    mqttc.loop_start()


def disconnect_mqtt_client(client_type):

    mqttc = _mqtt_client(client_type)

    if mqttc is None:
        return

    if mqttc.is_connected():
        if client_type == 'listener': unsubscribe()

        if report_status_flag:
            msg_info = mqttc.publish(f'{project_topic}clients/'+
                                     f'{mqttc._client_id.decode()}', None,
                                     retain=True, qos=2)
            msg_info.wait_for_publish(timeout=1)

        mqttc.loop_stop()
        mqttc.disconnect()

    if client_type == 'publisher': 
        global mqttp
        mqttp = None

    if client_type == 'listener': 
        global mqttl
        mqttl = None


def parse_config_file(config_file):
    
    config = configparser.ConfigParser()

    with open(config_file, 'r') as f:
        config.read_file(f)

        if config.has_option('PROJECT', 'name'):
            global project_name, project_topic
            project_name = config.get('PROJECT', 'name')
            project_topic = project_name+'/'

        if config.has_section('NETWORK'):

            if config.has_option('NETWORK', 'host'):
                mqtt_dict['host'] = config.get('NETWORK', 'host')

            if config.has_option('NETWORK', 'port'):
                mqtt_dict['port'] = int(config.get('NETWORK', 'port'))

            if config.has_option('NETWORK', 'username'):
                mqtt_dict['username'] = config.get('NETWORK', 'username')

            if config.has_option('NETWORK', 'password'):
                mqtt_dict['password'] = config.get('NETWORK', 'password')


def resend_retained_messages():
    if len(retained_messages) > 0:
        for rt, rm in retained_messages.copy():
            publish(rt, rm, retain=True)


def clear_retained_messages(topic=None):
    
    if topic is None:
        for rt, _ in retained_messages.copy():
            mqttp.publish(project_topic+rt, retain=True, qos=2)
            del retained_messages[0]
    
    else:
        for i, (rt, _) in enumerate(retained_messages):
            if rt == topic:
                del retained_messages[i]
                break
        mqttp.publish(project_topic+topic, retain=True, qos=2)


########################## public API - old functions #########################
###############################################################################
def init(client_name: str, expconfig=None, loglevel=logging.INFO,
         fileloglevel=None, publisher=True, listener=False, connect=True, 
         report_status=False, pingpong=False, unique=False, logdir='log',
         mqttloglevel=logging.INFO):
    '''Initialize libABCD in a single line.

    Parameters
    ----------
    client_name : str
        This will initialize a MQTT client with id `{client_name}_{pid}`. If 
        configured, the log files will be named `{client_name}.log`.
    expconfig: str or Path, optional
        The path to a configuration file. See libABCD.parse_config_file for
        details on how this file should look like. Default: None.
    loglevel: int, optional
        The logging level for the console logger. Default: logging.INFO.
    fileloglevel: int, optional
        The logging level for the file logger. If None (default), no file log
        will be created.
    publisher: bool, optional
        Whether to create a MQTT client to publish messages. Default: True.
    listener: bool, optional
        Whether to create a MQTT client to listen to messages. Default: False.
    connect: bool, optional
        Whether to connect the created MQTT client(s) to the broker. Default:
        True.
    report_status: bool, optional
        Whether to send a retained message to the topic 
        `{project_topic}clients/{client_id}` with payload `on` when the client
        connects and remove it when it disconnects. Default: False.
    pingpong: bool, optional
        [deprecated since v2.2] Whether to subscribe to the topic
        `{project_name}pings` and add a `pong` callback. Default: False.
    unique: bool, optional
        [obsolete since v2.2] 
    logdir: str or Path, optional
        The path where the file log is saved. Default: 'log'.
    mqttloglevel: int
        The logging level for the ABCDHandler. All logs with this or higher
        level will be sent via MQTT. Default: logging.INFO.
    '''


    global initialized_flag
    if initialized_flag:
        raise ABCDError('libABCD already initialized. This can only be done once per session.')
    
    global app_name, app_name_pid, report_status_flag
    app_name = client_name
    app_name_pid = app_name + '_' + pid
    report_status_flag = report_status

    if expconfig:
        parse_config_file(expconfig)

    setup_root_logger(console_level=loglevel, 
                      file_level=fileloglevel, file_dir=logdir,
                      mqtt_level=mqttloglevel, mqtt_topic='logs')

    if unique:
        logger.warning('"unique" feature is deprecated and has no effect.')

    if publisher: init_mqtt_client('publisher')
    if listener: init_mqtt_client('listener')

    if pingpong:
        logger.warning(f'"pingpong" feature is functional but will be deprecated. Consider using "report_status=True" and letting the broker handle the ping-pong.')
        if not listener or not publisher:
            logger.error('Both `publisher` and `listener` need to be True for pingpong to work.')
        else: add_callback('pings', _pingpong_callback)

    if connect:
        if publisher: connect_mqtt_client('publisher')
        if listener: connect_mqtt_client('listener')

    initialized_flag = True


def disconnect():
    disconnect_mqtt_client('listener')
    disconnect_mqtt_client('publisher')


def publish(topic, payload=None, retain=False, wait=True, timeout=None, 
            verbose=False, **kwargs):
    '''Publish a message in a MQTT topic.
    
    The message will be formatted according to libABCD format: the message will
    be a dictionary containing the following keys: 'from', 'timestamp' and
    'payload'.

    Parameters
    ----------
    topic: str
        the MQTT topic for the message. Will be prepended by the 
        `project_topic`.
    payload: Any, optional
        the actual message. Should be JSON-formattable. Default: None.
    retain: bool, optional
        whether to add the retain flag to the message. Default: False.
    wait: bool, optional
        whether to wait until the message is published. Default: True.
    timeout: float or None, optional
        if `wait` is True, how long to wait for the message to be published.
        If None (default) or negative, wait forever.
    verbose: bool, optional
        whether to print the message and topic to the console. Default: False.
    
    Other parameters
    ----------------
    **kwargs: optional
        additional keyword arguments will be passed to Client.publish().

    Raises
    ------
    ABCDError
        if the libABCD publisher is not initialized.
    '''

    if mqttp:
        msg = _format_msg(payload)
        if retain:  
            if len(retained_messages) > 0:
                for i, (rt,rm) in enumerate(retained_messages):
                    if rt == topic: 
                        del(retained_messages[i])
                        break

            retained_messages.append((topic, msg))

        ptopic = project_topic + topic
        if verbose: print(f'> Publishing: "{payload}" to {ptopic}')

        try:
            msg_info = mqttp.publish(f'{ptopic}', msg, retain=retain, qos=2,
                                     **kwargs)
            
        except Exception as e:
            logger.error(f'Following exception raised trying to publish: {e}')
    
        else:
            if wait:
                msg_info.wait_for_publish(timeout=timeout)

    else:
        raise ABCDError('mqtt publisher was not initialized. Cannot publish.')


def add_callback(topic, func, allow_empty_payload=False):
    
    def _msg_callback(mqttc: mqtt.Client, userdata, msg: mqtt.MQTTMessage):

        # decode and json-load msg
        dmsg = msg.payload.decode()

        if dmsg == '':
            if not allow_empty_payload:
                return      # ignore empty payloads
            jmsg = None

        else:
            try:
                jmsg = json.loads(dmsg)
            except Exception as e:
                logger.warning(f'Could not json-load following payload: {dmsg}. Exception: {e}.')
                return

            if 'from' not in jmsg or 'timestamp' not in jmsg \
                                                    or 'payload' not in jmsg:
                logger.warning(f'Wrong format in following message: {jmsg}. Nothing will happen.')
                return
        
        try:
            func(jmsg, msg.topic)
        except Exception as e:
            logger.exception(f'Exception raised when processing message {jmsg}.')


    if mqttl:

        # subscribe and add callback
        subscribe(topic)
        logger.debug(f'Adding callback to topic {project_topic}{topic}')
        mqttl.message_callback_add(f'{project_topic}{topic}', _msg_callback)

    else:
        raise ABCDError('Cannot "add_callback": mqtt listener not initialized.')


def subscribe(topic):
    
    if mqttl:
        
        topic = f'{project_topic}{topic}'

        if topic in subscriptions: return

        subscriptions.append(topic)

        # subscribe to topic
        mqttl.subscribe(topic, qos=2)

    else:
        raise ABCDError('Cannot "subscribe": listener was not initialized.')


def unsubscribe(topic=None):
    
    if not mqttl:
        return

    if topic is None:
        # unsubscribe from all topics
        for t in subscriptions.copy():
            mqttl.message_callback_remove(t)
            mqttl.unsubscribe(t)
            subscriptions.remove(t)
        return
    
    topic = f'{project_topic}{topic}'
    if topic in subscriptions: 
        mqttl.message_callback_remove(topic)
        mqttl.unsubscribe(topic)
        subscriptions.remove(topic)
