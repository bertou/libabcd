import logging, os, sys, time
from logging.handlers import TimedRotatingFileHandler
import libABCD


class ABCDHandler(logging.Handler):

    def __init__(self) -> None:
        super().__init__(level=logging.INFO)
        self.name = 'mqtt_handler'
        self.topic = None

    def emit(self, record: logging.LogRecord):

        if libABCD.mqttp is None:
            return
        
        # do not propagate mqtt logs to avoid infinite recursion
        if 'mqtt.' in record.name:
            return

        if self.topic is None:
            raise libABCD.ABCDError('ABCDHandler topic is not set')

        topic = f'{self.topic}/{record.name.replace('.', '/')}'

        try:
            libABCD.publish(topic, {'level': record.levelname, 
                                    'msg': record.msg})
        except Exception:
            self.handleError(record)


# disable annoying DEBUG messages coming from asyncio
# (python 3.12.3)
logging.getLogger("asyncio").setLevel(logging.INFO)


# format for console and file handlers
logging.Formatter.converter = time.gmtime   # set logging in UTC
formatter = logging.Formatter(
    '%(asctime)s - %(name)-20s - %(levelname)-8s - %(message)s')

# disable "PING" logging
pingfilter = logging.Filter()
pingfilter.filter = lambda r: 'PINGRE' not in r.getMessage()

# default handlers
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(formatter)
console_handler.name = 'console_handler'
console_handler.setLevel(logging.WARNING)
console_handler.addFilter(pingfilter)
hdlr_dict = {
    'console': console_handler,
    'file':    None,
    'mqtt':    ABCDHandler()
}

# root logger
rlogger = logging.getLogger()
rlogger.setLevel(logging.DEBUG)
rlogger.addHandler(hdlr_dict['console'])
rlogger.addHandler(hdlr_dict['mqtt'])

def init_file_handler(log_file_path):
    
    fhdlr = hdlr_dict['file']
    if fhdlr is not None:
        raise libABCD.ABCDError('File handler was already initialized. Cannot re-initialize, sorry.')

    fhdlr = TimedRotatingFileHandler(log_file_path, when='midnight',    
                                                 utc=True, delay=True)
    fhdlr.setFormatter(formatter)
    fhdlr.name = 'file_handler'
    hdlr_dict['file'] = fhdlr

    rlogger.addHandler(hdlr_dict['file'])


def set_handler_level(htype: str, new_level):
    '''Change the level of the chosen handler.'''
    hdlr = hdlr_dict[htype]
    hdlr.setLevel(new_level)


def set_formatter(new_formatter: logging.Formatter):
    '''Change the formatter for both the console and file handlers.'''
    hdlr_dict['console'].setFormatter(new_formatter)
    try:
        hdlr_dict['file'].setFormatter(new_formatter)
    except AttributeError: pass


def set_mqtt_log_topic(new_topic):
    hdlr_dict['mqtt'].topic = new_topic
