.. _reference:

#################
libABCD reference
#################

:Release: |release|
:Date: |today|

This reference page details functions, modules, and objects included in 
libABCD, describing what they are and what they do.

libABCD quick initialization
============================

The following function initializes everything with a single command:

.. autosummary::
    :toctree: generated/

    libABCD.init
