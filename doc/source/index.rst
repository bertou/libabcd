.. libABCD documentation master file, created by
   sphinx-quickstart on Thu Jun 13 15:52:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

############################################################
Autonomous Bariloche Central DAQ (``libABCD``) documentation
############################################################

:Version: |release|
:Date: |today|

This library handles two crucial aspects of any experiment that consists of 
multiple clients taking data: the communication between clients and the log 
information.

Clients communicate using the MQTT protocol. We recommend using the
`mosquitto <https://mosquitto.org/>`_ MQTT broker.

Logs are handled by each client, but the `ABCDHandler` logging handler sends
the relevant information through MQTT, allowing for a centralized monitoring.


Quick start
-----------

Import and initialize the library::

   >>> import libABCD
   >>> libABCD.init('my_daq')

Send the message "take_data" to the topic "detector1"::

   >>> libABCD.publish('detector1', 'take_data')

Disconnect::

   >>> libABCD.disconnect()

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   reference
