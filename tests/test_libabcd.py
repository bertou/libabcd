import json, logging, os, pytest, time
import libABCD
from importlib import reload
from paho.mqtt import client

cwd = os.path.dirname(os.path.abspath(__file__))

@pytest.fixture(scope='class')
def init_libabcd():
    
    def _init_libabcd(*args, **kwargs):
        libABCD.init(*args, **kwargs)
    
    yield _init_libabcd
    
    # cleanup
    libABCD.disconnect()
    reload(libABCD)


class TestInitWithoutMqtt:

    @pytest.fixture(scope='class', autouse=True)
    def libABCD_session(self, init_libabcd):
        init_libabcd(client_name='test', expconfig=cwd+'/test_config.ini', 
                     loglevel=logging.INFO, fileloglevel=logging.DEBUG, 
                     publisher=False, listener=False, connect=False,
                     report_status=False, pingpong=False, unique=False, 
                     logdir=cwd)

    @pytest.fixture(scope='class')
    def log_outputs(self):
        logger = libABCD.logger
        logger.debug('I am a debug message')
        logger.info('I am an info message')
        yield
        os.remove(f'{cwd}/test.log')

    def test_client_name(self):
        assert libABCD.app_name == 'test'

    def test_expconfig(self):
        assert libABCD.project_name == 'test_experiment'
        assert libABCD.mqtt_dict['host'] == 'test_host'
        assert libABCD.mqtt_dict['port'] == 12345
        assert libABCD.mqtt_dict['username'] == 'test_user'
        assert libABCD.mqtt_dict['password'] == 'p4$$w0rd'

    # def test_loglevel(self, capfd, log_outputs):
    #     captured = capfd.readouterr()
    #     assert 'debug' not in captured.out
    #     assert 'info' in captured.out

    def test_debug_message_in_log_file(self, log_outputs):
        with open(f'{cwd}/test.log') as f:
            assert 'I am a debug message' in f.read()


@pytest.fixture(scope='class')
def mqtt_tester():
    mqtt_tester = MqttTester()
    yield mqtt_tester
    del mqtt_tester


class MqttTester:

    def __init__(self) -> None:
        self.mqttc = client.Client(client.CallbackAPIVersion.VERSION2,
                                   client_id='test_listener', clean_session=True)
        self.mqttc.connect('localhost')
        self.mqttc.loop_start()
        self.last_msg = None
        self.sub = None
        self.waiting = False
    
    def _callback(self, client, userdata, message):
        try:
            dmsg = json.loads(message.payload.decode())
        except: return
        self.last_msg = dmsg
        self.waiting = False

    def listen_to_topic(self, topic):
        self.sub = topic
        self.mqttc.subscribe(topic)
        self.mqttc.message_callback_add(topic, self._callback)

    def reset(self):
        self.mqttc.message_callback_remove(self.sub)
        self.mqttc.unsubscribe(self.sub)
        self.last_msg = None
        self.waiting = False

    def __del__(self):
        self.mqttc.loop_stop()
        self.mqttc.disconnect()
        del self.mqttc


@pytest.fixture
def listen_to_topic(mqtt_tester):
    
    def _listen_to_topic(topic):
        mqtt_tester.listen_to_topic(topic)
        mqtt_tester.waiting = True
    
    yield _listen_to_topic
    mqtt_tester.reset()


class TestInitWithMqtt:

    @pytest.fixture(scope='class', autouse=True)
    def libABCD_session(self, init_libabcd):
        init_libabcd(client_name='test', expconfig=cwd+'/test_mqtt.ini', 
                     loglevel=logging.INFO, fileloglevel=None, 
                     publisher=True, listener=True, connect=True,
                     report_status=True, pingpong=True, unique=True, 
                     logdir=None)


    def test_publisher(self, mqtt_tester, listen_to_topic):
        listen_to_topic('test_mqtt/test_topic')
        libABCD.publish('test_topic', 'test message')
        start_time = time.time()
        while mqtt_tester.waiting:
            if time.time() - start_time > .1:
                assert False, 'No message received after 0.1 s'
        assert 'test' in mqtt_tester.last_msg['from']
        assert mqtt_tester.last_msg['payload'] == 'test message'


    # def test_listener(self, mqtt_tester):
    #     # mqtt_tester.mqttc.publish()
    #     pass


    def test_connect(self):
        start_time = time.time()
        while not libABCD.mqttp.is_connected():
            if time.time() - start_time > .1:
                assert False, 'Publisher not connected after .1 s'
   
        while not libABCD.mqttl.is_connected():
            if time.time() - start_time > .1:
                assert False, 'Listener not connected after .1 s'


    def test_report_status(self, listen_to_topic, mqtt_tester):
        listen_to_topic(f'test_mqtt/clients/test_{libABCD.pid}')
        start_time = time.time()
        while mqtt_tester.waiting:
            if time.time() - start_time > .1:
                assert False, 'No message received after .1 s'
        assert mqtt_tester.last_msg['payload'] == 'on'

        listen_to_topic(f'test_mqtt/clients/test_{libABCD.pid}_listener')
        start_time = time.time()
        while mqtt_tester.waiting:
            if time.time() - start_time > .1:
                assert False, 'No message received after .1 s'
        assert mqtt_tester.last_msg['payload'] == 'on'


    # def test_pingpong(self):
    #     pass

    # def test_unique(self):
    #     pass


def test_libabcd_can_be_initialized_only_once(init_libabcd):
    '''Test if libABCD.name can only be set once'''

    # act
    init_libabcd("first", publisher=False)
    with pytest.raises(libABCD.ABCDError, match='already initialized'):
        init_libabcd("second")

    # assert
    assert libABCD.app_name == 'first'


@pytest.fixture
def init_mqtt_client():

    ct = None
    # arrange
    def _init_client(client_type):
        nonlocal ct
        ct = client_type
        libABCD.init_mqtt_client(client_type, 'test_client')

    yield _init_client

    libABCD.disconnect_mqtt_client(ct)


@pytest.fixture
def connect_mqtt_client(init_mqtt_client):
    def _connect_client(client_type):
        init_mqtt_client(client_type)
        libABCD.connect_mqtt_client(client_type)
    
    yield _connect_client


def test_mqtt_listener_initialized(init_mqtt_client):
    
    init_mqtt_client("listener")
    assert libABCD.mqttl._client_id.decode() == 'test_client'


def test_mqtt_publisher_initialized(init_mqtt_client):
    
    init_mqtt_client("publisher")
    assert libABCD.mqttp._client_id.decode() == 'test_client'


def test_mqtt_listener_connected(connect_mqtt_client):
    connect_mqtt_client('listener')

    start_time = time.time()
    while not libABCD.mqttl.is_connected():
        if time.time() - start_time > .1:
            assert False, 'Listener not connected after .1s'


def test_mqtt_publisher_connected(connect_mqtt_client):
    connect_mqtt_client('publisher')

    start_time = time.time()
    while not libABCD.mqttp.is_connected():
        if time.time() - start_time > .1:
            assert False, 'Publisher not connected after .1s'


def test_publish(mqtt_tester, listen_to_topic, connect_mqtt_client):
    connect_mqtt_client('publisher')
    listen_to_topic('test')

    libABCD.publish('test', 'test message')

    start_time = time.time()
    while mqtt_tester.waiting:
        if time.time() - start_time > .1:
            assert False, 'No message received after 0.1 s'


def test_subscribe_and_add_callback(connect_mqtt_client, mqtt_tester, capsys):
    connect_mqtt_client('listener')
    libABCD.add_callback('listen_test', lambda x,t: print(t,x['payload']))
    mqtt_tester.mqttc.publish('listen_test', libABCD._format_msg('hi'))
    
    start_time = time.time()
    captured = capsys.readouterr()
    while captured.out == '':
        if time.time() - start_time > .1:
            assert False, 'No message received after 0.1 s'
        captured = capsys.readouterr()
    
    assert 'hi' in captured.out