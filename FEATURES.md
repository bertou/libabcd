What libABCD can do:

- configure the root logger of the calling python interpreter:
    - set the level to DEBUG
    - set all logging time information to UTC
    - set a specific format for messages
    - add a console handler
    - if requested, add a file handler (will rotate every 24 hours, with UTC time)
    - if requested, add a MQTT handler (send some log messages through MQTT)

- create and manage two MQTT clients:
    - a LISTENER that can only SUBSCRIBE and ADD_CALLBACK
    - a PUBLISHER that can only PUBLISH

- additional and optional features:
    - `report_status` to enable other MQTT apps to track how many clients are connected (or whether a specific client is connected)
    - `pingpong` to enable an automatic "pong" response (deprecated)